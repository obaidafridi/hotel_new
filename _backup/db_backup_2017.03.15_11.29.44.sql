-- -------------------------------------------
SET AUTOCOMMIT=0;
START TRANSACTION;
SET SQL_QUOTE_SHOW_CREATE = 1;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
-- -------------------------------------------
-- -------------------------------------------
-- START BACKUP
-- -------------------------------------------
-- -------------------------------------------
-- TABLE `account_ledger`
-- -------------------------------------------
DROP TABLE IF EXISTS `account_ledger`;
CREATE TABLE IF NOT EXISTS `account_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dr` int(11) NOT NULL,
  `cr` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `chkin_id` varchar(15) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `dr` (`dr`,`cr`),
  KEY `cr` (`cr`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `account_name`
-- -------------------------------------------
DROP TABLE IF EXISTS `account_name`;
CREATE TABLE IF NOT EXISTS `account_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `guest_comp_id` int(11) DEFAULT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `account_type_id` (`account_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `account_type`
-- -------------------------------------------
DROP TABLE IF EXISTS `account_type`;
CREATE TABLE IF NOT EXISTS `account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `assignments`
-- -------------------------------------------
DROP TABLE IF EXISTS `assignments`;
CREATE TABLE IF NOT EXISTS `assignments` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `country`
-- -------------------------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) NOT NULL,
  `country_currancy` varchar(50) NOT NULL,
  `currancy_sign` varchar(10) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `day_end`
-- -------------------------------------------
DROP TABLE IF EXISTS `day_end`;
CREATE TABLE IF NOT EXISTS `day_end` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `today_date` date NOT NULL,
  `active_date` date NOT NULL,
  `night_post` varchar(10) NOT NULL DEFAULT '0',
  `last_night_post` datetime DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_branches`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_branches`;
CREATE TABLE IF NOT EXISTS `hms_branches` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_address` varchar(255) NOT NULL,
  `branch_phone` varchar(20) NOT NULL,
  `branch_fax` varchar(30) NOT NULL,
  `branch_email` varchar(50) NOT NULL,
  `ntn_no` varchar(20) DEFAULT NULL,
  `gst_no` varchar(20) DEFAULT NULL,
  `room_limit` int(11) NOT NULL DEFAULT '0',
  `hotel_id` int(11) NOT NULL,
  `active_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `appkey` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_checkin_info`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_checkin_info`;
CREATE TABLE IF NOT EXISTS `hms_checkin_info` (
  `chkin_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_id` int(11) NOT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `chkin_date` datetime NOT NULL,
  `chkout_date` datetime NOT NULL,
  `drop_service` varchar(2) DEFAULT NULL,
  `flight_name` varchar(30) DEFAULT NULL,
  `flight_time` time DEFAULT NULL,
  `total_days` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `room_type` varchar(20) NOT NULL,
  `room_name` varchar(20) NOT NULL,
  `guest_company_id` int(11) DEFAULT NULL,
  `rate_type` int(11) NOT NULL,
  `total_person` int(2) NOT NULL,
  `total_charges` double NOT NULL,
  `amount_paid` int(11) DEFAULT NULL,
  `chkout_status` varchar(2) NOT NULL,
  `chkin_user_id` int(11) DEFAULT NULL,
  `chkout_user_id` int(11) DEFAULT NULL,
  `guest_status_id` int(11) NOT NULL,
  `guest_folio_no` int(11) NOT NULL,
  `sale_person_id` int(11) NOT NULL,
  `comming_from` varchar(20) DEFAULT NULL,
  `next_destination` varchar(20) DEFAULT NULL,
  `newspaper_id` int(11) DEFAULT NULL,
  `rate` int(11) NOT NULL,
  `cash` varchar(15) DEFAULT NULL,
  `credit_card` varchar(15) DEFAULT NULL,
  `debit_card` varchar(15) DEFAULT NULL,
  `btc` varchar(15) DEFAULT NULL,
  `comments` varchar(300) DEFAULT NULL,
  `gst` varchar(2) NOT NULL,
  `gst_show` int(11) NOT NULL DEFAULT '0',
  `reg_no` varchar(15) DEFAULT NULL,
  `bed_tax` varchar(2) DEFAULT NULL,
  `prev_night` varchar(2) NOT NULL,
  `night_post_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`chkin_id`),
  KEY `guest_id` (`guest_id`),
  KEY `room_id` (`room_id`),
  KEY `newspaper_id` (`newspaper_id`),
  KEY `company_id` (`guest_company_id`),
  KEY `guest_status_id` (`guest_status_id`),
  KEY `sale_person_id` (`sale_person_id`),
  KEY `branch_id` (`branch_id`),
  KEY `chkin_user_id` (`chkin_user_id`),
  KEY `chkout_user_id` (`chkout_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_city`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_city`;
CREATE TABLE IF NOT EXISTS `hms_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_company_info`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_company_info`;
CREATE TABLE IF NOT EXISTS `hms_company_info` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_no` int(11) DEFAULT NULL,
  `comp_name` varchar(100) NOT NULL,
  `comp_contact_person` varchar(80) DEFAULT NULL,
  `person_designation` varchar(70) DEFAULT NULL,
  `person_mobile` varchar(25) DEFAULT NULL,
  `comp_address` varchar(255) DEFAULT NULL,
  `comp_phone` varchar(25) DEFAULT NULL,
  `comp_fax` varchar(25) DEFAULT NULL,
  `comp_email` varchar(100) DEFAULT NULL,
  `comp_website` varchar(50) DEFAULT NULL,
  `comp_allow_credit` varchar(2) DEFAULT NULL,
  `rate_to` varchar(120) DEFAULT NULL,
  `rate_from` varchar(120) DEFAULT NULL,
  `rate_date` datetime DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`comp_id`),
  UNIQUE KEY `comp_name` (`comp_name`),
  KEY `country_id` (`country_id`),
  KEY `user_id` (`user_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_exchange_rate`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_exchange_rate`;
CREATE TABLE IF NOT EXISTS `hms_exchange_rate` (
  `excange_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `sign` varchar(10) NOT NULL,
  `exchabge_rate` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`excange_rate_id`),
  KEY `country_id` (`country_id`),
  KEY `user_id` (`user_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_flight_info`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_flight_info`;
CREATE TABLE IF NOT EXISTS `hms_flight_info` (
  `flight_id` int(11) NOT NULL AUTO_INCREMENT,
  `flight_name` varchar(20) NOT NULL,
  `flight_arrival` time NOT NULL,
  `flight_departure` time NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`flight_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_floor`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_floor`;
CREATE TABLE IF NOT EXISTS `hms_floor` (
  `floor_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(50) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`floor_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_guest_info`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_guest_info`;
CREATE TABLE IF NOT EXISTS `hms_guest_info` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_no` int(11) DEFAULT NULL,
  `guest_salutation_id` int(11) NOT NULL,
  `guest_name` varchar(50) NOT NULL,
  `guest_address` varchar(255) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `guest_phone` varchar(15) DEFAULT NULL,
  `guest_country_id` int(11) NOT NULL,
  `guest_mobile` varchar(25) NOT NULL,
  `guest_identity_id` int(11) NOT NULL,
  `guest_identity_no` varchar(25) DEFAULT NULL,
  `guest_identity_issu` date NOT NULL,
  `guest_identiy_expire` date NOT NULL,
  `guest_gender` varchar(10) NOT NULL,
  `guest_email` varchar(50) NOT NULL,
  `guest_company_id` int(11) DEFAULT NULL,
  `guest_remarks` varchar(50) NOT NULL,
  `guest_dob` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`guest_id`),
  KEY `guest_country_id` (`guest_country_id`),
  KEY `guest_company_id` (`guest_company_id`),
  KEY `guest_salutation_id` (`guest_salutation_id`),
  KEY `user_id` (`user_id`),
  KEY `branch_id` (`branch_id`),
  KEY `guest_identity_id` (`guest_identity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_guest_ledger`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_guest_ledger`;
CREATE TABLE IF NOT EXISTS `hms_guest_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chkin_id` int(11) DEFAULT NULL,
  `guest_name` varchar(30) NOT NULL,
  `room_status` varchar(2) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `chkin_date` datetime NOT NULL,
  `chkout_date` datetime NOT NULL,
  `c_date` datetime NOT NULL,
  `c_time` time NOT NULL,
  `service_id` int(11) NOT NULL,
  `remarks` varchar(30) DEFAULT NULL,
  `debit` double NOT NULL,
  `credit` double NOT NULL,
  `balance` double NOT NULL,
  `cash_paid` int(11) NOT NULL,
  `credit_card` varchar(2) NOT NULL,
  `credit_card_no` int(11) NOT NULL,
  `btc` varchar(2) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `late_out` varchar(2) NOT NULL,
  `day_close` int(11) NOT NULL DEFAULT '0',
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`),
  KEY `service_id` (`service_id`),
  KEY `company_id` (`company_id`),
  KEY `branch_id` (`branch_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_guest_status`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_guest_status`;
CREATE TABLE IF NOT EXISTS `hms_guest_status` (
  `guest_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_description` varchar(50) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`guest_status_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_identity`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_identity`;
CREATE TABLE IF NOT EXISTS `hms_identity` (
  `identity_id` int(11) NOT NULL AUTO_INCREMENT,
  `identity_description` varchar(30) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`identity_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_newspapers`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_newspapers`;
CREATE TABLE IF NOT EXISTS `hms_newspapers` (
  `newspaper_id` int(11) NOT NULL AUTO_INCREMENT,
  `newspaper_name` varchar(30) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`newspaper_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_rate_type`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_rate_type`;
CREATE TABLE IF NOT EXISTS `hms_rate_type` (
  `rate_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_name` varchar(50) NOT NULL,
  `days` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`rate_type_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_reservation_info`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_reservation_info`;
CREATE TABLE IF NOT EXISTS `hms_reservation_info` (
  `reservation_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_type` varchar(2) NOT NULL,
  `group_name` varchar(30) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `to_person` varchar(100) NOT NULL,
  `designation` varchar(120) NOT NULL,
  `chkin_date` datetime NOT NULL,
  `chkout_date` datetime NOT NULL,
  `c_date` date NOT NULL,
  `total_days` int(11) NOT NULL,
  `pick_service` varchar(2) DEFAULT NULL,
  `flight_name` varchar(20) DEFAULT NULL,
  `flight_time` time DEFAULT NULL,
  `drop_service` varchar(2) DEFAULT NULL,
  `drop_flight_name` varchar(20) DEFAULT NULL,
  `drop_flight_time` time DEFAULT NULL,
  `client_salutation_id` int(11) NOT NULL,
  `client_name` varchar(30) NOT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `client_country_id` int(11) NOT NULL,
  `client_mobile` varchar(30) NOT NULL,
  `client_phone` varchar(20) DEFAULT NULL,
  `guest_mobile` varchar(25) DEFAULT NULL,
  `guest_phone` varchar(25) DEFAULT NULL,
  `client_email` varchar(50) DEFAULT NULL,
  `client_identity_id` int(11) NOT NULL,
  `client_identity_no` varchar(25) DEFAULT NULL,
  `reservation_status` int(4) NOT NULL,
  `room_type` varchar(20) DEFAULT NULL,
  `room_name` varchar(50) DEFAULT NULL,
  `cancel_status` varchar(2) NOT NULL,
  `cancel_date` datetime NOT NULL,
  `cancel_reason` varchar(50) NOT NULL,
  `cancel_by` int(11) NOT NULL,
  `chkin_status` varchar(2) NOT NULL,
  `noshow_status` varchar(2) NOT NULL,
  `client_remarks` varchar(50) NOT NULL,
  `room_charges` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `gst` varchar(2) NOT NULL,
  `advance` int(11) DEFAULT NULL,
  `mode_of_payment` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sale_person_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`reservation_id`),
  KEY `company_id` (`company_id`),
  KEY `client_country_id` (`client_country_id`),
  KEY `client_identity_id` (`client_identity_id`),
  KEY `reservation_status` (`reservation_status`),
  KEY `client_salutation_id` (`client_salutation_id`),
  KEY `sale_person_id` (`sale_person_id`),
  KEY `branch_id` (`branch_id`),
  KEY `user_id` (`user_id`),
  KEY `cancel_by` (`cancel_by`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_reservation_status`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_reservation_status`;
CREATE TABLE IF NOT EXISTS `hms_reservation_status` (
  `res_id` int(11) NOT NULL AUTO_INCREMENT,
  `res_description` varchar(30) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`res_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_room_master`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_room_master`;
CREATE TABLE IF NOT EXISTS `hms_room_master` (
  `mst_room_id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_room_name` int(11) NOT NULL,
  `mst_floor_id` int(11) NOT NULL,
  `mst_roomtypeid` int(11) NOT NULL,
  `mst_room_remarks` varchar(50) NOT NULL,
  `mst_room_adults` int(11) NOT NULL,
  `mst_room_childs` int(11) NOT NULL,
  `mst_room_status` varchar(2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`mst_room_id`),
  UNIQUE KEY `mst_room_name` (`mst_room_name`),
  KEY `mst_floor_id` (`mst_floor_id`),
  KEY `mst_roomtypeid` (`mst_roomtypeid`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_room_shift`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_room_shift`;
CREATE TABLE IF NOT EXISTS `hms_room_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chkin_id` int(11) NOT NULL,
  `guest_id` int(11) NOT NULL,
  `old_room_id` int(11) NOT NULL,
  `new_room_id` int(11) NOT NULL,
  `reason` varchar(400) DEFAULT NULL,
  `shift_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_room_type`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_room_type`;
CREATE TABLE IF NOT EXISTS `hms_room_type` (
  `room_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(50) NOT NULL,
  `adults` int(11) NOT NULL,
  `childs` int(11) NOT NULL,
  `room_rate` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`room_type_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_room_type_rate`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_room_type_rate`;
CREATE TABLE IF NOT EXISTS `hms_room_type_rate` (
  `room_type_rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type_id` int(11) NOT NULL,
  `rate_type_id` int(11) NOT NULL,
  `room_rate` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `room_rate_status` varchar(30) NOT NULL,
  `room_comments` varchar(50) NOT NULL,
  `comp_allow_gst` int(2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`room_type_rate_id`),
  KEY `rate_type_id` (`rate_type_id`),
  KEY `room_type_id` (`room_type_id`),
  KEY `company_id` (`company_id`),
  KEY `branch_id` (`branch_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_sale_person`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_sale_person`;
CREATE TABLE IF NOT EXISTS `hms_sale_person` (
  `sale_person_id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_person_name` varchar(30) NOT NULL,
  `sale_person_comm` int(11) NOT NULL,
  `is_active` varchar(3) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`sale_person_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_salutation`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_salutation`;
CREATE TABLE IF NOT EXISTS `hms_salutation` (
  `salutation_id` int(11) NOT NULL AUTO_INCREMENT,
  `salutation_name` varchar(50) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`salutation_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_service_gst`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_service_gst`;
CREATE TABLE IF NOT EXISTS `hms_service_gst` (
  `gst_id` int(11) NOT NULL AUTO_INCREMENT,
  `gst_service_id` int(11) NOT NULL,
  `gst_percent` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`gst_id`),
  KEY `gst_service_id` (`gst_service_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hms_services`
-- -------------------------------------------
DROP TABLE IF EXISTS `hms_services`;
CREATE TABLE IF NOT EXISTS `hms_services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `acc_no` int(11) DEFAULT NULL,
  `service_code` varchar(20) DEFAULT NULL,
  `service_description` varchar(30) NOT NULL,
  `servise_type` varchar(4) NOT NULL,
  `service_rate` int(11) NOT NULL,
  `order_by` varchar(80) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`service_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `hotel_title`
-- -------------------------------------------
DROP TABLE IF EXISTS `hotel_title`;
CREATE TABLE IF NOT EXISTS `hotel_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `application_title` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `logo_image` varchar(150) NOT NULL,
  `bg_image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `itemchildren`
-- -------------------------------------------
DROP TABLE IF EXISTS `itemchildren`;
CREATE TABLE IF NOT EXISTS `itemchildren` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `items`
-- -------------------------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `logins`
-- -------------------------------------------
DROP TABLE IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `settings`
-- -------------------------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `value` varchar(50) NOT NULL,
  `fieldtype` varchar(15) DEFAULT NULL,
  `htmlcode` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit` (`unit`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `user`
-- -------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `hotel_branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `hotel_branch_id` (`hotel_branch_id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE `user_admin`
-- -------------------------------------------
DROP TABLE IF EXISTS `user_admin`;
CREATE TABLE IF NOT EXISTS `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- -------------------------------------------
-- TABLE DATA account_ledger
-- -------------------------------------------
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('1','13','9','2500','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('2','13','8','425','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('3','13','3','1','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('4','12','13','500','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('5','15','9','5000','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('6','15','8','850','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('7','15','3','1','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('8','18','9','3000','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('9','18','8','510','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('10','18','3','1','1','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('11','19','9','2600','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('12','19','8','0','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('13','19','3','1','2','0000-00-00 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('14','21','9','2000','3','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('15','21','3','1','3','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('16','22','9','3000','4','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('17','22','3','2','4','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('18','12','22','3000','4','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('19','23','9','6000','5','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('20','23','8','1020','5','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('21','23','3','2','5','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('22','12','23','6000','5','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('23','24','9','2300','6','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('24','24','3','1','6','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('25','25','9','2300','7','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('26','25','3','1','7','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('27','26','9','1800','8','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('28','26','3','1','8','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('29','27','9','2800','9','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('30','27','3','3','9','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('31','28','9','2000','10','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('32','28','3','1','10','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('33','29','9','2500','11','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('34','29','3','1','11','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('35','30','9','2500','12','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('36','30','3','1','12','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('37','31','9','2500','13','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('38','31','3','1','13','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('39','12','31','3000','13','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('40','32','9','2800','14','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('41','32','3','2','14','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('42','33','9','2800','15','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('43','33','3','2','15','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('44','34','9','2800','16','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('45','34','3','2','16','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('46','35','9','2800','17','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('47','35','3','2','17','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('48','36','9','5100','18','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('49','36','3','3','18','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('50','37','9','2200','19','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('51','37','3','2','19','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('52','38','9','2200','20','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('53','38','3','2','20','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('54','39','9','2200','21','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('55','39','8','374','21','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('56','39','3','1','21','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('57','40','9','2600','22','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('58','40','3','2','22','2017-03-14 00:00:00','0000-00-00 00:00:00');
INSERT INTO `account_ledger` (`id`,`dr`,`cr`,`amount`,`chkin_id`,`created_date`,`updated_date`) VALUES
('59','12','40','2500','22','2017-03-14 00:00:00','0000-00-00 00:00:00');



-- -------------------------------------------
-- TABLE DATA account_name
-- -------------------------------------------
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('1','PIA','','1','2017-03-13 15:25:52');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('2','Gst Service','','4','2017-03-13 15:37:52');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('3','Bed Tax','','4','2017-03-13 15:38:51');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('4','Room Rent Service','','4','2017-03-13 15:39:35');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('5','Extra Bed Service','','4','2017-03-13 15:40:02');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('6','Advance Service','','4','2017-03-13 15:40:17');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('7','Bed Tax Service','','4','2017-03-13 15:40:28');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('8','GST','','4','2017-03-13 15:50:12');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('9','Room Rent','','4','2017-03-13 15:50:34');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('10','Extra Bed','','4','2017-03-13 15:54:15');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('11',' Advance','','4','2017-03-13 15:54:26');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('12','Advance','','4','2017-03-13 15:55:22');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('13','Zain Kazmi-03336506505','','2','2017-03-13 16:28:19');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('14','Individual','','1','2017-03-13 16:41:10');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('15','Syed Hassan -030022222222222','','2','2017-03-13 16:42:39');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('16','Paid','','4','2017-03-13 16:59:06');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('17','Bill To Company','','4','2017-03-13 17:00:13');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('18','Syed Kazmi-03336506505','','2','2017-03-14 17:10:16');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('19','fgdfg-3012312','','2','2017-03-14 17:27:50');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('20','Zain Kazmi-033333333333333','','2','2017-03-14 17:28:29');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('21','Zain ul Hassan-03329146554','','2','2017-03-14 17:58:05');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('22','Abdul basit-03004430016','','2','2017-03-14 18:27:25');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('23','Maqsood sab-03125982598','','2','2017-03-14 18:36:27');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('24','M iqbal -0333422222','','2','2017-03-14 18:44:13');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('25','Abdul Shoorabay-03333','','2','2017-03-14 19:35:10');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('26','Bilal sab-03465119057','','2','2017-03-14 20:03:05');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('27','Ameer Ali-03425363270','','2','2017-03-14 20:05:44');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('28','Zhang Mingyue-03111153688','','2','2017-03-14 20:07:45');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('29','Mr. Ishfaq-03124480488','','2','2017-03-14 20:15:51');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('30','Muneeb Arshad-03218814007','','2','2017-03-14 20:51:33');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('31','Zaka Ullah Awan-03234527322','','2','2017-03-14 23:05:40');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('32','Muhammad aslam-222322','','2','2017-03-14 23:24:56');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('33','Bibi Zahra-23562','','2','2017-03-14 23:25:59');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('34','Ghulam fatima-3201','','2','2017-03-14 23:27:24');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('35','Muhammad Shahzad-1234','','2','2017-03-14 23:28:23');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('36','Muhammad Altaf-321032','','2','2017-03-14 23:29:18');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('37','Abid Hussain-03022805641','','2','2017-03-14 23:42:09');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('38','Zahid Naseer-03146828241','','2','2017-03-14 23:49:24');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('39','rana abdur rehman-0343840007','','2','2017-03-15 03:53:07');
INSERT INTO `account_name` (`id`,`name`,`guest_comp_id`,`account_type_id`,`created_date`) VALUES
('40','Saeed ul Haq-03331615999','','2','2017-03-15 10:58:10');



-- -------------------------------------------
-- TABLE DATA assignments
-- -------------------------------------------
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Authority','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('FrontDesk','14','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Administrator','15','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Auditor','16','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Administrator','13','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Authority','15','','');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('taxcontroller','17','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Manager','2','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('FrontDesk','2','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Authority','2','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Administrator','2','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Auditor','2','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Administrator','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Auditor','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('FrontDesk','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('headadmin','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('Manager','1','','s:0:\"\";');
INSERT INTO `assignments` (`itemname`,`userid`,`bizrule`,`data`) VALUES
('taxcontroller','1','','s:0:\"\";');



-- -------------------------------------------
-- TABLE DATA country
-- -------------------------------------------
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('1','Pakistan','PK','NULL','0092','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('2','Afghani','Af','NULL','0093','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('3','Algeria','DA','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('6','Angola','Kz','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('9','Albania','EC$','NULL','0','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('10','Argentina','A','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('11','Armenia','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('13','Australia','A$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('14','Austria','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('15','Azerbaijan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('16','British','B$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('17','Bahrain','BD','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('18','Bangladesh','Tk','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('19','Barbados','Bds$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('20','Belarus','BR','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('21','Belgium','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('24','Bermuda','Bd$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('25','Bhutan','Nu','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('26','Bolivia','Bs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('27','Bosnia and Herzegovina','KM','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('30','Brazil','R$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('32','Brunei','B$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('33','Bulgaria','Lv','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('35','Burundi','Fbu','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('36','Cambodia','CR','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('37','Cameroon','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('38','Canada','Can$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('42','Chad','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('43','Chile','Ch$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('44','China','Y','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('49','Colombia','Col$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('51','Congo','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('54','Costa Rica','/C','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('55','Deutsch','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('56','Croatia (Hrvatska)','HRK','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('57','Cuba','Cu$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('58','Cyprus','£C','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('59','Czech Republic','Kc','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('60','Denmark','Dkr','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('63','Dominican Republic','RD$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('64','East Timor','Rp','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('65','Ecuador','S/','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('66','Egypt','£E','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('67','El Salvador','¢','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('69','Eritrea','Nfa','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('70','Estonia','KR','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('71','Ethiopia','Br','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('73','Faroe Islands','Dkr','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('74','Fiji Islands','F$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('75','Finland','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('76','France','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('80','Gabon','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('81','Gambia, The','D','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('82','Georgia','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('83','Germany','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('84','Ghana','¢','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('85','Gibraltar','£G','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('86','Greece','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('87','Greenland','Dkr','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('88','Grenada','EC$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('90','Guam','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('91','Guatemala','Q','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('92','Guinea','PG','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('93','Guinea-Bissau','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('94','Guyana','G$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('95','Haiti','G$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('96','Heard and McDonald Islands','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('97','Honduras','L','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('98','Hungary','Ft','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('99','Iceland','Ikr','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('100','INDIA','Rs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('101','Indonesia','Rp','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('102','Iran','Rls','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('103','Iraq','ID','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('104','Ireland','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('105','Islands (British)','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('106','Israel','NIS','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('107','Italy','Lit','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('108','Jamaica','J$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('109','Japan','¥','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('110','Jordan','JD','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('111','Kazakhstan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('112','Kenya','K Sh','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('113','Kiribati','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('114','Korea','W','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('115','Korea, North','Wn','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('116','Kuwait','KD','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('117','Kyrgyzstan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('118','Laos','KN','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('119','Latvia','Ls','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('120','Lebanon','L.L.','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('121','Lesotho','L, lp, M','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('122','Liberia','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('123','Libya','LD','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('124','Liechtenstein','SwF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('125','Lithuania','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('127','Macedonia, Former Yugoslav','MKD','NULL','00','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('128','Madagascar','FMG','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('129','Malawi','MK','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('130','Malaysia','RM','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('131','Maldives','Rf','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('132','Mali','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('133','Malta','Lm','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('135','Martinique','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('136','Mauritania','UM','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('137','Mauritius','Mau Rs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('138','Mayotte','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('139','Mexico','Mex$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('140','Micronesia','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('141','Moldova','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('142','Monaco','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('143','Mongolia','Tug','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('144','Montserrat','EC$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('145','Morocco','DH','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('146','Mozambique','Mt','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('147','Myanmar','K','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('148','Namibia','N$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('149','Nauru','A$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('150','Nepal','NRs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('151','Netherlands','Ant.f. .','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('153','New Caledonia','CFPF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('154','New Zealand','NZ$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('155','Nicaragua','C$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('156','Niger','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('157','Nigeria',' N','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('158','Niue','NZ$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('159','Norfolk Island','A$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('160','Palestine','$','NULL','0000000','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('161','Norway','NKr','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('162','Oman','RO','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('163','Pakistani','Rs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('165','Panama','B','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('166','Papua new Guinea','K','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('167','Paraguay','/G','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('168','Peru','S/','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('169','Philippines','P','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('171','Poland','z - 1','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('172','Portugal','¤','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('173','Puerto Rico','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('174','Qatar','QR','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('176','Romania','L','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('177','Russia','R','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('178','Rwanda','RF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('181','Saint Lucia','EC$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('183','Saint Vincent ','EC$','NULL','000','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('185','Serbia','Lit','NULL','381','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('187','Saudi Arabia','SR1s','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('188','Senegal','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('190','Sierra Leone','Le','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('191','Singapore','S$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('192','Slovakia','Sk','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('193','Slovenia','S1T','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('195','Somalia','So. Sh','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('196','South Africa','R','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('197','Spain','Ptas','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('198','Sri Lanka','SLRs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('199','Sudan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('202','Swaziland','L, pl. , E','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('203','Sweden','kr or Sk','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('204','Switzerland','SwF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('205','Syria','£S','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('206','Taiwan','NT$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('207','Tajikistan','100 dirams','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('208','Tanzania','TSh','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('209','Thailand','Bht or bt','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('210','Togo','CFAF','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('214','Tunisia','TD','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('215','Turkey','TL','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('216','Turkmenistan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('219','Uganda','Ush','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('220','Ukraine','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('221','UAE','Dh','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('223','USA','$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('225','Uruguay','Nur$','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('226','Uzbekistan','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('229','Venezuela','Bs','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('230','Vietnam','','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('234','Yemen','YR1s','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('235','Yugoslavia','Din','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('236','Zambia','ZK','NULL','NULL','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('237','Zimbabwe','kjhg','NULL','0000','7');
INSERT INTO `country` (`country_id`,`country_name`,`country_currancy`,`currancy_sign`,`country_code`,`branch_id`) VALUES
('238','United States','','NULL','NULL','7');



-- -------------------------------------------
-- TABLE DATA day_end
-- -------------------------------------------
INSERT INTO `day_end` (`id`,`today_date`,`active_date`,`night_post`,`last_night_post`,`date`,`branch_id`) VALUES
('1','2017-03-14','2017-03-14','0','2017-03-13 00:00:00','2017-03-15 11:19:32','3');



-- -------------------------------------------
-- TABLE DATA hms_branches
-- -------------------------------------------
INSERT INTO `hms_branches` (`branch_id`,`branch_address`,`branch_phone`,`branch_fax`,`branch_email`,`ntn_no`,`gst_no`,`room_limit`,`hotel_id`,`active_date`,`expiry_date`,`appkey`) VALUES
('3','Potohar Hotel, Waris Khan Stop, Murree Road, Rawalpindi.','051-5770110','','info@potoharhotel.com','','','40','4','2017-03-01','2018-03-01','');



-- -------------------------------------------
-- TABLE DATA hms_checkin_info
-- -------------------------------------------
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('1','1','','2017-03-14 00:00:00','2017-03-14 00:00:00','N','','00:00:00','1','5','2','5','1','1','1','3511','','Y','14','','1','1','3','','','','3000','Y','N','N','N','','Y','1','1','Y','N','0000-00-00','3','2017-03-14 17:10:28');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('2','2','','2017-03-14 00:00:00','2017-03-14 00:00:00','N','','00:00:00','1','1','2','1','2','1','1','2601','','Y','14','','1','2','3','','','','2600','Y','Y','N','N','','Y','2','2','Y','N','0000-00-00','3','2017-03-14 17:28:00');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('3','4','','2017-03-14 00:00:00','2017-03-14 00:00:00','N','','00:00:00','1','7','1','7','2','1','1','2001','','Y','14','','1','3','3','','','','2000','Y','N','N','N','','N','3','4896','Y','N','0000-00-00','3','2017-03-14 17:58:05');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('4','5','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','28','1','28','2','1','2','3002','3000','N','14','','1','4','3','','','','3000','Y','N','N','N','','N','4','4901','Y','N','0000-00-00','3','2017-03-14 18:27:25');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('5','6','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','15','3','15','2','1','2','7022','6000','N','14','','1','5','3','','','','6000','Y','N','N','N','','Y','5','4898','Y','N','0000-00-00','3','2017-03-14 18:36:27');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('6','7','','2017-03-14 00:00:00','2017-03-14 00:00:00','N','','00:00:00','1','30','1','30','1','1','1','2301','','Y','14','','1','6','3','','','','2300','N','N','N','Y','','N','6','4900','Y','N','0000-00-00','3','2017-03-14 18:44:13');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('7','8','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','19','1','19','1','1','1','2301','','N','14','','1','7','3','','','','2300','N','N','N','Y','','N','7','4902','Y','N','0000-00-00','3','2017-03-14 19:35:10');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('8','9','','2017-03-15 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','21','1','21','2','1','1','1801','5000','N','15','','1','8','3','','','','1800','Y','N','N','N','','N','8','4833','Y','N','0000-00-00','3','0000-00-00 00:00:00');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('9','10','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','6','4','6','2','1','3','2803','','N','14','','1','9','3','','','','2800','Y','N','N','N','','N','9','4874','Y','N','0000-00-00','3','2017-03-14 20:05:44');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('10','11','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','11','1','11','2','1','1','2001','','N','14','','1','10','3','','','','2000','Y','N','N','N','','N','10','4883','Y','N','0000-00-00','3','2017-03-14 20:07:45');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('11','12','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','30','1','30','2','1','1','2501','','N','14','','1','11','3','','','','2500','Y','N','N','N','','N','11','4744','Y','N','0000-00-00','3','2017-03-14 20:15:51');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('12','13','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','4','1','4','2','1','1','2501','','N','14','','1','12','3','','','','2500','Y','N','N','N','','N','12','4903','Y','N','0000-00-00','3','2017-03-14 20:51:33');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('13','14','','2017-03-15 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','2','2','2','2','1','1','2501','3000','N','15','','1','13','3','','','','2500','Y','N','N','N','','N','13','4905','Y','N','0000-00-00','3','0000-00-00 00:00:00');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('14','15','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','9','1','9','1','1','2','2802','','N','14','','1','14','3','','','','2800','Y','N','N','Y','','N','14','4904','Y','N','0000-00-00','3','2017-03-14 23:24:56');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('15','16','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','12','2','12','1','1','2','2802','','N','14','','1','15','3','','','','2800','N','N','N','Y','','N','15','4904A','Y','N','0000-00-00','3','2017-03-14 23:25:59');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('16','17','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','17','2','17','1','1','2','2802','','N','14','','1','16','3','','','','2800','N','N','N','Y','','N','16','4904B','Y','N','0000-00-00','3','2017-03-14 23:27:24');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('17','18','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','22','2','22','1','1','2','2802','','N','14','','1','17','3','','','','2800','N','N','N','Y','','N','17','4904C','Y','N','0000-00-00','3','2017-03-14 23:28:23');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('18','19','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','16','4','16','1','1','3','5103','','N','14','','1','18','3','','','','5100','N','N','N','Y','','N','18','4904D','Y','N','0000-00-00','3','2017-03-14 23:29:18');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('19','20','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','8','2','8','2','1','2','2202','','N','14','','1','19','3','','','','2200','Y','N','N','N','','N','19','4891','Y','N','0000-00-00','3','2017-03-14 23:42:09');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('20','21','','2017-03-14 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','3','1','3','2','1','2','2202','','N','14','','1','20','3','','','','2200','Y','N','N','N','','N','20','4866','Y','N','0000-00-00','3','2017-03-14 23:49:24');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('21','22','','2017-03-15 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','18','1','18','2','1','1','2575','','Y','14','','1','21','3','','','','2200','Y','N','N','N','','Y','21','4906','Y','N','0000-00-00','3','2017-03-15 03:53:32');
INSERT INTO `hms_checkin_info` (`chkin_id`,`guest_id`,`reservation_id`,`chkin_date`,`chkout_date`,`drop_service`,`flight_name`,`flight_time`,`total_days`,`room_id`,`room_type`,`room_name`,`guest_company_id`,`rate_type`,`total_person`,`total_charges`,`amount_paid`,`chkout_status`,`chkin_user_id`,`chkout_user_id`,`guest_status_id`,`guest_folio_no`,`sale_person_id`,`comming_from`,`next_destination`,`newspaper_id`,`rate`,`cash`,`credit_card`,`debit_card`,`btc`,`comments`,`gst`,`gst_show`,`reg_no`,`bed_tax`,`prev_night`,`night_post_date`,`branch_id`,`created_time`) VALUES
('22','23','3','2017-03-15 00:00:00','2017-03-15 00:00:00','N','','00:00:00','1','1','2','1','2','1','2','3044','2500','Y','14','','1','22','3','','','','2600','Y','Y','N','N','','N','22','123','Y','N','0000-00-00','3','2017-03-15 10:59:11');



-- -------------------------------------------
-- TABLE DATA hms_company_info
-- -------------------------------------------
INSERT INTO `hms_company_info` (`comp_id`,`acc_no`,`comp_name`,`comp_contact_person`,`person_designation`,`person_mobile`,`comp_address`,`comp_phone`,`comp_fax`,`comp_email`,`comp_website`,`comp_allow_credit`,`rate_to`,`rate_from`,`rate_date`,`country_id`,`branch_id`,`user_id`) VALUES
('1','1','PIA','PIA','','','','','','','','N','PIA','Potohar Hotel','2017-03-13 15:40:43','1','3','15');
INSERT INTO `hms_company_info` (`comp_id`,`acc_no`,`comp_name`,`comp_contact_person`,`person_designation`,`person_mobile`,`comp_address`,`comp_phone`,`comp_fax`,`comp_email`,`comp_website`,`comp_allow_credit`,`rate_to`,`rate_from`,`rate_date`,`country_id`,`branch_id`,`user_id`) VALUES
('2','14','Individual','Individual','','','','','','','','N','MCE','Pothohar Hotel','','1','3','15');



-- -------------------------------------------
-- TABLE DATA hms_flight_info
-- -------------------------------------------
INSERT INTO `hms_flight_info` (`flight_id`,`flight_name`,`flight_arrival`,`flight_departure`,`branch_id`) VALUES
('1','pk-101','13:00:00','00:00:00','3');
INSERT INTO `hms_flight_info` (`flight_id`,`flight_name`,`flight_arrival`,`flight_departure`,`branch_id`) VALUES
('2','Pk-102','03:00:00','00:00:00','3');
INSERT INTO `hms_flight_info` (`flight_id`,`flight_name`,`flight_arrival`,`flight_departure`,`branch_id`) VALUES
('3','PK-103','16:00:10','13:00:12','3');



-- -------------------------------------------
-- TABLE DATA hms_floor
-- -------------------------------------------
INSERT INTO `hms_floor` (`floor_id`,`description`,`branch_id`) VALUES
('1','1st Floor','3');
INSERT INTO `hms_floor` (`floor_id`,`description`,`branch_id`) VALUES
('2','2nd Floor','3');
INSERT INTO `hms_floor` (`floor_id`,`description`,`branch_id`) VALUES
('3','3rd Floor','3');



-- -------------------------------------------
-- TABLE DATA hms_guest_info
-- -------------------------------------------
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('1','18','1','Syed Kazmi','f-7, isb','','','1','03336506505','1','55555-5555555-5','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('2','19','1','fgdfg','','','','1','3012312','1','45345-3453453-4','2017-03-14','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('3','20','1','Zain Kazmi','G-8, Isb','','','1','033333333333333','1','11111-1111111-1','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('4','21','1','Zain ul Hassan','Peshawar','','','1','03329146554','1','17301-3820755-5','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('5','22','1','Abdul basit','','','','1','03004430016','1','35202-2422330-7','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('6','23','1','Maqsood sab','','','','1','03125982598','1','82203-7700834-7','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('7','24','1','M iqbal ','','','','1','0333422222','2','42225','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('8','25','1','Abdul Shoorabay','','','','1','03333','2','321112','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('9','26','1','Bilal sab','','','','1','03465119057','1','71501-2185984-9','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','15');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('10','27','1','Ameer Ali','','','','1','03425363270','1','71501-3965141-3','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('11','28','1','Zhang Mingyue','','','','44','03111153688','2','G38844711','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('12','29','1','Mr. Ishfaq','','','','1','03124480488','1','81202-8098672-1','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('13','30','1','Muneeb Arshad','','','','1','03218814007','1','36502-0313104-5','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('14','31','1','Zaka Ullah Awan','','','','1','03234527322','1','35201-3190325-5','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','15');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('15','32','1','Muhammad aslam','','','','1','222322','2','214124579005','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('16','33','1','Bibi Zahra','','','','1','23562','2','214124579006','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('17','34','1','Ghulam fatima','','','','1','3201','2','214124579008','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('18','35','1','Muhammad Shahzad','','','','1','1234','2','214124579009','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('19','36','1','Muhammad Altaf','','','','1','321032','2','214124579002','0000-00-00','0000-00-00','M','','1','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('20','37','1','Abid Hussain','','','','1','03022805641','1','31201-7227566-3','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('21','38','1','Zahid Naseer','','','','1','03146828241','1','33401-0415897-3','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('22','39','1','rana abdur rehman','','','','1','0343840007','1','35404-6476129-9','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');
INSERT INTO `hms_guest_info` (`guest_id`,`acc_no`,`guest_salutation_id`,`guest_name`,`guest_address`,`city_id`,`guest_phone`,`guest_country_id`,`guest_mobile`,`guest_identity_id`,`guest_identity_no`,`guest_identity_issu`,`guest_identiy_expire`,`guest_gender`,`guest_email`,`guest_company_id`,`guest_remarks`,`guest_dob`,`branch_id`,`user_id`) VALUES
('23','40','1','Saeed ul Haq','RWP','','0512323568','1','03331615999','1','43434-3243343-4','0000-00-00','0000-00-00','M','','2','','0000-00-00','3','14');



-- -------------------------------------------
-- TABLE DATA hms_guest_ledger
-- -------------------------------------------
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('1','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-10-28 00:00:00','17:10:28','3','0','3000','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('2','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:10:28','17:10:28','1','0','510','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('3','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:10:28','17:10:28','2','0','1','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('4','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:11:10','17:03:00','4','1','500','0','0','0','0','0','','1','14','','4','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('5','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:11:45','17:03:00','5','1','0','1000','0','0','0','0','','1','14','','4','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('6','1','Syed Kazmi','D','5','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:12:23','17:12:30','6','1','0','3011','0','1','0','0','0','1','14','','5','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('7','2','fgdfg','D','1','2017-03-14 00:00:00','2017-03-14 00:00:00','0000-00-00 00:00:00','17:28:00','3','0','2600','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('8','2','fgdfg','D','1','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:28:00','17:28:00','1','0','0','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('9','2','fgdfg','D','1','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:28:00','17:28:00','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('10','2','fgdfg','D','1','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:28:55','17:03:00','4','2','5464','0','0','0','0','0','','2','14','','4','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('11','2','fgdfg','D','1','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:29:15','17:29:18','6','2','0','8065','0','1','0','0','0','2','14','','5','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('12','3','Zain ul Hassan','D','7','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:58:05','17:58:05','3','0','2000','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('13','3','Zain ul Hassan','D','7','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 17:58:05','17:58:05','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('14','4','Abdul basit','O','28','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:27:25','18:27:25','3','0','3000','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('15','4','Abdul basit','O','28','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:27:25','18:27:25','2','0','2','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('16','4','Abdul basit','O','28','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:27:25','18:27:25','5','0','0','3000','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('17','5','Maqsood sab','O','15','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:36:28','18:36:28','3','0','6000','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('18','5','Maqsood sab','O','15','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:36:28','18:36:28','1','0','1020','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('19','5','Maqsood sab','O','15','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:36:28','18:36:28','2','0','2','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('20','5','Maqsood sab','O','15','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 18:36:28','18:36:28','5','0','0','6000','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('21','6','M iqbal ','D','30','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 18:44:13','18:44:13','3','0','2300','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('22','6','M iqbal ','D','30','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 18:44:13','18:44:13','2','0','1','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('23','7','Abdul Shoorabay','O','19','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 19:35:11','19:35:11','3','0','2300','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('24','7','Abdul Shoorabay','O','19','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 19:35:11','19:35:11','2','0','1','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('25','6','M iqbal ','D','30','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 19:52:27','19:52:34','6','1','0','2301','0','0','1','0','2','1','14','','5','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('26','8','Bilal sab','O','21','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:03:05','20:03:05','3','0','1800','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('27','8','Bilal sab','O','21','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:03:05','20:03:05','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('28','9','Ameer Ali','O','6','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:05:44','20:05:44','3','0','2800','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('29','9','Ameer Ali','O','6','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:05:44','20:05:44','2','0','3','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('30','10','Zhang Mingyue','O','11','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:07:45','20:07:45','3','0','2000','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('31','10','Zhang Mingyue','O','11','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:07:45','20:07:45','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('32','11','Mr. Ishfaq','O','30','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:15:51','20:15:51','3','0','2500','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('33','11','Mr. Ishfaq','O','30','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:15:51','20:15:51','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('34','12','Muneeb Arshad','O','4','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:51:33','20:51:33','3','0','2500','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('35','12','Muneeb Arshad','O','4','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 20:51:33','20:51:33','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('36','13','Zaka Ullah Awan','O','2','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:05:41','23:05:41','3','0','2500','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('37','13','Zaka Ullah Awan','O','2','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:05:41','23:05:41','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('38','13','Zaka Ullah Awan','O','2','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:05:41','23:05:41','5','0','0','3000','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('39','14','Muhammad aslam','O','9','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:24:56','23:24:56','3','0','2800','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('40','14','Muhammad aslam','O','9','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:24:56','23:24:56','2','0','2','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('41','15','Bibi Zahra','O','12','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:25:59','23:25:59','3','0','2800','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('42','15','Bibi Zahra','O','12','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:25:59','23:25:59','2','0','2','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('43','16','Ghulam fatima','O','17','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:27:24','23:27:24','3','0','2800','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('44','16','Ghulam fatima','O','17','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:27:24','23:27:24','2','0','2','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('45','17','Muhammad Shahzad','O','22','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:28:24','23:28:24','3','0','2800','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('46','17','Muhammad Shahzad','O','22','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:28:24','23:28:24','2','0','2','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('47','18','Muhammad Altaf','O','16','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:29:18','23:29:18','3','0','5100','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('48','18','Muhammad Altaf','O','16','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:29:18','23:29:18','2','0','3','0','0','0','0','0','0','1','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('49','19','Abid Hussain','O','8','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:42:09','23:42:09','3','0','2200','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('50','19','Abid Hussain','O','8','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:42:09','23:42:09','2','0','2','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('51','3','Zain ul Hassan','D','7','2017-03-14 00:00:00','2017-03-14 00:00:00','2017-03-14 23:46:59','23:47:30','7','2','2001','0','2001','0','0','0','3','2','14','','5','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('52','20','Zahid Naseer','O','3','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:49:25','23:49:25','3','0','2200','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('53','20','Zahid Naseer','O','3','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14 23:49:25','23:49:25','2','0','2','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('54','21','rana abdur rehman','D','18','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 03:53:32','03:53:32','3','0','2200','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('55','21','rana abdur rehman','D','18','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 03:53:32','03:53:32','1','0','374','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('56','21','rana abdur rehman','D','18','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 03:53:32','03:53:32','2','0','1','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('57','21','rana abdur rehman','D','18','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-15 09:41:03','09:41:24','6','2','0','2500','0','1','0','0','0','2','14','','5','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('58','22','Saeed ul Haq','D','1','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 10:59:11','10:59:11','3','0','2600','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('59','22','Saeed ul Haq','D','1','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 10:59:11','10:59:11','2','0','2','0','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('60','22','Saeed ul Haq','D','1','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-14 10:59:11','10:59:11','5','0','0','2500','0','0','0','0','0','2','14','N','1','3');
INSERT INTO `hms_guest_ledger` (`id`,`chkin_id`,`guest_name`,`room_status`,`room_id`,`chkin_date`,`chkout_date`,`c_date`,`c_time`,`service_id`,`remarks`,`debit`,`credit`,`balance`,`cash_paid`,`credit_card`,`credit_card_no`,`btc`,`company_id`,`user_id`,`late_out`,`day_close`,`branch_id`) VALUES
('61','22','Saeed ul Haq','D','1','2017-03-15 00:00:00','2017-03-15 00:00:00','2017-03-15 11:01:18','11:01:24','6','022','0','102','0','1','0','0','0','2','14','','5','3');



-- -------------------------------------------
-- TABLE DATA hms_guest_status
-- -------------------------------------------
INSERT INTO `hms_guest_status` (`guest_status_id`,`status_description`,`branch_id`) VALUES
('1','Reguler','1');
INSERT INTO `hms_guest_status` (`guest_status_id`,`status_description`,`branch_id`) VALUES
('2','VIP','1');



-- -------------------------------------------
-- TABLE DATA hms_identity
-- -------------------------------------------
INSERT INTO `hms_identity` (`identity_id`,`identity_description`,`branch_id`) VALUES
('1','CNIC','1');
INSERT INTO `hms_identity` (`identity_id`,`identity_description`,`branch_id`) VALUES
('2','Pasport','1');
INSERT INTO `hms_identity` (`identity_id`,`identity_description`,`branch_id`) VALUES
('3','Driveing Licence','7');
INSERT INTO `hms_identity` (`identity_id`,`identity_description`,`branch_id`) VALUES
('5','Test Identity','7');



-- -------------------------------------------
-- TABLE DATA hms_newspapers
-- -------------------------------------------
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('1','Jang','1');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('2','Down','1');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('3','Insaf','1');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('4','Express','1');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('5','Daily Jang','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('6','Daily Dawn','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('7','Nahi Baat','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('8','Daily News','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('9','Pakistna Post','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('10','Family Magzene','7');
INSERT INTO `hms_newspapers` (`newspaper_id`,`newspaper_name`,`branch_id`) VALUES
('11','Juriat','7');



-- -------------------------------------------
-- TABLE DATA hms_rate_type
-- -------------------------------------------
INSERT INTO `hms_rate_type` (`rate_type_id`,`rate_name`,`days`,`branch_id`) VALUES
('1','Daily','1','1');



-- -------------------------------------------
-- TABLE DATA hms_reservation_info
-- -------------------------------------------
INSERT INTO `hms_reservation_info` (`reservation_id`,`res_type`,`group_name`,`company_id`,`to_person`,`designation`,`chkin_date`,`chkout_date`,`c_date`,`total_days`,`pick_service`,`flight_name`,`flight_time`,`drop_service`,`drop_flight_name`,`drop_flight_time`,`client_salutation_id`,`client_name`,`client_address`,`client_country_id`,`client_mobile`,`client_phone`,`guest_mobile`,`guest_phone`,`client_email`,`client_identity_id`,`client_identity_no`,`reservation_status`,`room_type`,`room_name`,`cancel_status`,`cancel_date`,`cancel_reason`,`cancel_by`,`chkin_status`,`noshow_status`,`client_remarks`,`room_charges`,`discount`,`gst`,`advance`,`mode_of_payment`,`user_id`,`sale_person_id`,`branch_id`) VALUES
('1','I','','2','Individual','self','2017-03-14 00:00:00','2017-03-15 00:00:00','2017-03-14','1','N','','00:00:00','N','','00:00:00','1','Zain Kazmi','G-8, Isb','1','033333333333','','0333333333333333','','','1','11111-1111111-1','2','3','','N','2017-03-14 18:39:37','','14','N','Y','','4500','','N','','1','14','1','3');
INSERT INTO `hms_reservation_info` (`reservation_id`,`res_type`,`group_name`,`company_id`,`to_person`,`designation`,`chkin_date`,`chkout_date`,`c_date`,`total_days`,`pick_service`,`flight_name`,`flight_time`,`drop_service`,`drop_flight_name`,`drop_flight_time`,`client_salutation_id`,`client_name`,`client_address`,`client_country_id`,`client_mobile`,`client_phone`,`guest_mobile`,`guest_phone`,`client_email`,`client_identity_id`,`client_identity_no`,`reservation_status`,`room_type`,`room_name`,`cancel_status`,`cancel_date`,`cancel_reason`,`cancel_by`,`chkin_status`,`noshow_status`,`client_remarks`,`room_charges`,`discount`,`gst`,`advance`,`mode_of_payment`,`user_id`,`sale_person_id`,`branch_id`) VALUES
('2','I','','2','Individual','Gm','2017-03-15 00:00:00','2017-03-16 00:00:00','2017-03-15','1','N','','00:00:00','N','','00:00:00','1','Ali','','1','1111','','0_______________','','','1','23100-1210___-_','2','1','7','N','2017-03-15 00:53:13','','14','N','N','','2200','','N','','1','14','1','3');
INSERT INTO `hms_reservation_info` (`reservation_id`,`res_type`,`group_name`,`company_id`,`to_person`,`designation`,`chkin_date`,`chkout_date`,`c_date`,`total_days`,`pick_service`,`flight_name`,`flight_time`,`drop_service`,`drop_flight_name`,`drop_flight_time`,`client_salutation_id`,`client_name`,`client_address`,`client_country_id`,`client_mobile`,`client_phone`,`guest_mobile`,`guest_phone`,`client_email`,`client_identity_id`,`client_identity_no`,`reservation_status`,`room_type`,`room_name`,`cancel_status`,`cancel_date`,`cancel_reason`,`cancel_by`,`chkin_status`,`noshow_status`,`client_remarks`,`room_charges`,`discount`,`gst`,`advance`,`mode_of_payment`,`user_id`,`sale_person_id`,`branch_id`) VALUES
('3','I','','2','Individual','MD','2017-03-15 00:00:00','2017-03-16 00:00:00','2017-03-15','1','N','','00:00:00','N','','00:00:00','1','Saeed ul Haq','RWP','1','03331615999','0512323568','03331615999_____','0512323568','','1','43434-3243343-4','1','3','','N','0000-00-00 00:00:00','','14','Y','N','jhg','4500','','N','','1','14','1','3');



-- -------------------------------------------
-- TABLE DATA hms_reservation_status
-- -------------------------------------------
INSERT INTO `hms_reservation_status` (`res_id`,`res_description`,`branch_id`) VALUES
('1','Confirm','1');
INSERT INTO `hms_reservation_status` (`res_id`,`res_description`,`branch_id`) VALUES
('2','Cancel','1');
INSERT INTO `hms_reservation_status` (`res_id`,`res_description`,`branch_id`) VALUES
('3','Normal','1');



-- -------------------------------------------
-- TABLE DATA hms_room_master
-- -------------------------------------------
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('1','101','1','2','Twin','2','0','D','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('2','102','1','2','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('3','103','1','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('4','104','1','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('5','105','1','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('6','106','1','4','','3','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('7','107','1','1','','1','0','R','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('8','108','1','2','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('9','109','1','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('10','110','1','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('11','111','1','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('12','112','1','2','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('13','213','2','1','','1','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('14','214','2','3','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('15','215','2','3','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('16','216','2','4','','3','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('17','217','2','2','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('18','218','2','1','','1','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('19','219','2','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('20','220','2','1','','1','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('21','221','2','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('22','222','2','2','','2','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('23','323','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('24','324','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('25','325','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('26','326','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('27','327','3','1','','1','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('28','328','3','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('29','329','3','1','','1','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('30','330','3','1','','1','0','O','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('31','331','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('32','332','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('33','333','3','2','','2','0','V','3');
INSERT INTO `hms_room_master` (`mst_room_id`,`mst_room_name`,`mst_floor_id`,`mst_roomtypeid`,`mst_room_remarks`,`mst_room_adults`,`mst_room_childs`,`mst_room_status`,`branch_id`) VALUES
('34','334','3','2','','2','0','V','3');



-- -------------------------------------------
-- TABLE DATA hms_room_shift
-- -------------------------------------------
INSERT INTO `hms_room_shift` (`id`,`chkin_id`,`guest_id`,`old_room_id`,`new_room_id`,`reason`,`shift_date`,`user_id`,`branch_id`) VALUES
('1','2','2','14','15','cleaning issue','2017-03-13 17:08:58','15','3');



-- -------------------------------------------
-- TABLE DATA hms_room_type
-- -------------------------------------------
INSERT INTO `hms_room_type` (`room_type_id`,`room_name`,`adults`,`childs`,`room_rate`,`branch_id`) VALUES
('1','Master','1','0','2500','3');
INSERT INTO `hms_room_type` (`room_type_id`,`room_name`,`adults`,`childs`,`room_rate`,`branch_id`) VALUES
('2','Twin','2','0','3000','3');
INSERT INTO `hms_room_type` (`room_type_id`,`room_name`,`adults`,`childs`,`room_rate`,`branch_id`) VALUES
('3','Suite','2','0','5000','3');
INSERT INTO `hms_room_type` (`room_type_id`,`room_name`,`adults`,`childs`,`room_rate`,`branch_id`) VALUES
('4','Triple Bed','3','0','4000','3');



-- -------------------------------------------
-- TABLE DATA hms_room_type_rate
-- -------------------------------------------
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('1','1','1','2500','1','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('2','2','1','3000','1','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('3','3','1','5000','1','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('4','4','1','4000','1','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('5','1','1','2200','2','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('6','2','1','2600','2','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('7','3','1','4500','2','C','comments','0','3','15');
INSERT INTO `hms_room_type_rate` (`room_type_rate_id`,`room_type_id`,`rate_type_id`,`room_rate`,`company_id`,`room_rate_status`,`room_comments`,`comp_allow_gst`,`branch_id`,`user_id`) VALUES
('8','4','1','3500','2','C','comments','0','3','15');



-- -------------------------------------------
-- TABLE DATA hms_sale_person
-- -------------------------------------------
INSERT INTO `hms_sale_person` (`sale_person_id`,`sale_person_name`,`sale_person_comm`,`is_active`,`branch_id`) VALUES
('1','PotoharHotel Sales Person','1','Yes','3');



-- -------------------------------------------
-- TABLE DATA hms_salutation
-- -------------------------------------------
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('1','Mr.','1');
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('2','Miss.','7');
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('3','Mrs.','7');
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('4','Mr. & Mrs.','7');
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('5','Dr.','7');
INSERT INTO `hms_salutation` (`salutation_id`,`salutation_name`,`branch_id`) VALUES
('6','Engr.','3');



-- -------------------------------------------
-- TABLE DATA hms_service_gst
-- -------------------------------------------
INSERT INTO `hms_service_gst` (`gst_id`,`gst_service_id`,`gst_percent`,`branch_id`) VALUES
('1','1','16','7');
INSERT INTO `hms_service_gst` (`gst_id`,`gst_service_id`,`gst_percent`,`branch_id`) VALUES
('2','1','17','3');



-- -------------------------------------------
-- TABLE DATA hms_services
-- -------------------------------------------
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('1','8','01','GST','Dr','17','service_id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('2','3','02','Bed Tax','Dr','0','id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('3','9','03','Room Rent','Dr','0','id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('4','10','04','Extra Bed','Dr','0','id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('5','12','05','Advance','Cr','0','id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('6','16','06','Paid','Cr','0','id','3');
INSERT INTO `hms_services` (`service_id`,`acc_no`,`service_code`,`service_description`,`servise_type`,`service_rate`,`order_by`,`branch_id`) VALUES
('7','17','07','Bill To Company','Dr','0','id','3');



-- -------------------------------------------
-- TABLE DATA hotel_title
-- -------------------------------------------
INSERT INTO `hotel_title` (`id`,`title`,`application_title`,`website`,`logo_image`,`bg_image`) VALUES
('1','MaalikSoft','HMS','http://www.maaliksoft.com','170313142012elodger.png','');
INSERT INTO `hotel_title` (`id`,`title`,`application_title`,`website`,`logo_image`,`bg_image`) VALUES
('4','Pothohar Hotel','Pothohar Hotel','http://potoharhotel.com/','170308141215logo.png','');



-- -------------------------------------------
-- TABLE DATA itemchildren
-- -------------------------------------------
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CompanyAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CompanyViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CountryAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','CountryViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ExchangeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ExchangeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','FlightsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','FlightsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','frontdeskmisc');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','GuestStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','hmcontrol@HmsbranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','hmcontrol@HmsbranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','hmcontrol@HoteltitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','hmcontrol@HoteltitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsBranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsBranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsFloorAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsFloorViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsRoomTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HmsRoomTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HotelTitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','HotelTitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','IdentityAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','IdentityViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','NewspapersAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','NewspapersViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RateTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RateTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReportsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReportsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReservationInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReservationInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReservationStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ReservationStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RoomMasterAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RoomTypeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','RoomTypeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','SalePersonAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','SalePersonViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ServiceGstAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ServiceGstViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ServicesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','UserAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Administrator','UserViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','RoomMasterAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','ServicesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Auditor','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CompanyAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CompanyViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CountryAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','CountryViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ExchangeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ExchangeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','FlightsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','FlightsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','GuestStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','hmcontrol@HmsbranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','hmcontrol@HmsbranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','hmcontrol@HoteltitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','hmcontrol@HoteltitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','HmsFloorAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','HmsFloorViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','HmsRoomTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','HmsRoomTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','IdentityAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','IdentityViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','NewspapersAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','NewspapersViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RateTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RateTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReportsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReportsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReservationInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReservationInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReservationStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ReservationStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RoomMasterAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RoomTypeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','RoomTypeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','SalePersonAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','SalePersonViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ServiceGstAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ServiceGstViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ServicesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Authority','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoInhouse');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoProcessDayClose');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInforoomsgrid');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoUpdateRoomStatus');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoAdministrating','CheckinInfoViewRegCard');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoViewing','CheckinInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoViewing','CheckinInfoCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoViewing','CheckinInfoIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoViewing','CheckinInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CheckinInfoViewing','CheckinInfoViewRegCard');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyAdministrating','CompanyView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyViewing','CompanyAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyViewing','CompanyCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyViewing','CompanyIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CompanyViewing','CompanyView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryAdministrating','CountryView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryViewing','CountryAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryViewing','CountryCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryViewing','CountryIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('CountryViewing','CountryView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsAdministrating','FlightsView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsViewing','FlightsAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsViewing','FlightsCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsViewing','FlightsIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FlightsViewing','FlightsView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','CompanyViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','CountryViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ExchangeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ExchangeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','FlightsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','frontdeskmisc');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','GuestInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','GuestStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','GuestStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','hmcontrol@HmsbranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','hmcontrol@HmsbranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','hmcontrol@HoteltitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','hmcontrol@HoteltitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsBranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsBranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsFloorAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsFloorViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsRoomTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HmsRoomTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HotelTitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','HotelTitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','IdentityViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','NewspapersViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','RateTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','RateTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ReservationInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ReservationStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ReservationStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','RoomTypeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','RoomTypeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','SalePersonViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('FrontDesk','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('frontdeskmisc','RoomMasterUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoAdministrating','GuestInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoViewing','GuestInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoViewing','GuestInfoCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestInfoViewing','GuestInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','CheckinInfoUpdateRoomStatus');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerCreateRoomPost');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerAdministrating','GuestLedgerView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerViewing','GuestLedgerCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestLedgerViewing','GuestLedgerView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusAdministrating','GuestStatusView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('GuestStatusViewing','GuestStatusView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CompanyAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CompanyViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CountryAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','CountryViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ExchangeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ExchangeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','FlightsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','FlightsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','frontdeskmisc');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','GuestStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','hmcontrol@HmsbranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','hmcontrol@HmsbranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','hmcontrol@HoteltitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','hmcontrol@HoteltitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsBranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsBranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsFloorAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsFloorViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsRoomTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HmsRoomTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HotelTitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','HotelTitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','IdentityAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','IdentityViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','NewspapersAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','NewspapersViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RateTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RateTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReportsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReportsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReservationInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReservationInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReservationStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ReservationStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RoomMasterAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RoomTypeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','RoomTypeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','SalePersonAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','SalePersonViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ServiceGstAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ServiceGstViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ServicesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('headadmin','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesAdministrating','hmcontrol@HmsbranchesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HmsbranchesViewing','hmcontrol@HmsbranchesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleAdministrating','hmcontrol@HoteltitleView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('hmcontrol@HoteltitleViewing','hmcontrol@HoteltitleView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesAdministrating','HmsBranchesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsBranchesViewing','HmsBranchesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorAdministrating','HmsFloorView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsFloorViewing','HmsFloorView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeDynamicPerson');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeAdministrating','HmsRoomTypeView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HmsRoomTypeViewing','HmsRoomTypeView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleAdministrating','HotelTitleView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('HotelTitleViewing','HotelTitleView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityAdministrating','IdentityView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityViewing','IdentityAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityViewing','IdentityCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityViewing','IdentityIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('IdentityViewing','IdentityView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CompanyAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CompanyViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CountryAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','CountryViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ExchangeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ExchangeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','FlightsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','FlightsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestLedgerAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestLedgerViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','GuestStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','hmcontrol@HmsbranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','hmcontrol@HmsbranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','hmcontrol@HoteltitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','hmcontrol@HoteltitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsBranchesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsBranchesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsFloorAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsFloorViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsRoomTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HmsRoomTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HotelTitleAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','HotelTitleViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','IdentityAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','IdentityViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','NewspapersAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','NewspapersViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RateTypeAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RateTypeViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReportsAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReportsViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReservationInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReservationInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReservationStatusAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ReservationStatusViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RoomMasterAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RoomMasterViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RoomTypeRateAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','RoomTypeRateViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','SalePersonAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','SalePersonViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ServiceGstAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ServiceGstViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ServicesAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('Manager','ServicesViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersAdministrating','NewspapersView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersViewing','NewspapersAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersViewing','NewspapersCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersViewing','NewspapersIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('NewspapersViewing','NewspapersView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeAdministrating','RateTypeView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RateTypeViewing','RateTypeView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsAdministrating','ReportsAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsAdministrating','ReportsIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsAdministrating','ReportsPanel');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsAdministrating','ReportsReport');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsAdministrating','ReportsView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReportsViewing','ReportsView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoReport');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoToday');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoAdministrating','ReservationInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoViewing','ReservationInfoAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoViewing','ReservationInfoCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoViewing','ReservationInfoUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationInfoViewing','ReservationInfoView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusAdministrating','ReservationStatusView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ReservationStatusViewing','ReservationStatusView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterAdministrating','RoomMasterView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterViewing','RoomMasterAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterViewing','RoomMasterIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomMasterViewing','RoomMasterView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateAdministrating','RoomTypeRateView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('RoomTypeRateViewing','RoomTypeRateView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonAdministrating','SalePersonView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonViewing','SalePersonAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonViewing','SalePersonCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonViewing','SalePersonIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('SalePersonViewing','SalePersonView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstAdministrating','ServiceGstView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServiceGstViewing','ServiceGstView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesAdministrating','ServicesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesViewing','ServicesAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesViewing','ServicesCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesViewing','ServicesIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('ServicesViewing','ServicesView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('taxcontroller','CheckinInfoAdministrating');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('taxcontroller','CheckinInfoViewing');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserAdmin');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserCreate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserDelete');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserIndex');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserUpdate');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserAdministrating','UserView');
INSERT INTO `itemchildren` (`parent`,`child`) VALUES
('UserViewing','UserView');



-- -------------------------------------------
-- TABLE DATA items
-- -------------------------------------------
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('Authority','2','','','');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('Administrator','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoViewRegCard','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoInhouse','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoUpdateRoomStatus','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInfoProcessDayClose','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CheckinInforoomsgrid','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CompanyAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('CountryAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ExchangeRateViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ExchangeRateAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FlightsAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestInfoAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerUpdate','0','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestStatusAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsBranchesAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsFloorAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HotelTitleAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('HmsRoomTypeDynamicPerson','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('IdentityAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('NewspapersAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RateTypeAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsPanel','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsReport','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReportsAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoReport','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoToday','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationInfoAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCreateRoomPost','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServiceGstViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('Auditor','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('FrontDesk','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('Manager','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ReservationStatusAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomMasterAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('RoomTypeRateAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('SalePersonAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('ServicesAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HmsbranchesAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('hmcontrol@HoteltitleAdmin','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCharts','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCodewise','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerbtc','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerpayBTC','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerViewBill','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerMerge','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerMergebills','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerSplit','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerDothis','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerMyprint','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCreateS','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerCheckOutPopup','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerTransferSelected','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerOldfolio','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerDayClose','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerDayClose2','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerUpdate_','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerNightpost','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('GuestLedgerDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('frontdeskmisc','1','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('taxcontroller','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('headadmin','2','','','s:0:\"\";');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserViewing','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserAdministrating','1','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserView','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserCreate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserUpdate','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserDelete','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserIndex','0','','','N;');
INSERT INTO `items` (`name`,`type`,`description`,`bizrule`,`data`) VALUES
('UserAdmin','0','','','N;');



-- -------------------------------------------
-- TABLE DATA logins
-- -------------------------------------------
INSERT INTO `logins` (`id`,`name`,`username`,`password`,`date`) VALUES
('1','rashid ali','rashid','123','2013-01-01 14:53:26');
INSERT INTO `logins` (`id`,`name`,`username`,`password`,`date`) VALUES
('2','Faisal','admin','admin','2013-01-26 18:07:52');



-- -------------------------------------------
-- TABLE DATA settings
-- -------------------------------------------
INSERT INTO `settings` (`id`,`unit`,`description`,`value`,`fieldtype`,`htmlcode`) VALUES
('1','taxcontrol','Tax Controll Yes/No','0','','');
INSERT INTO `settings` (`id`,`unit`,`description`,`value`,`fieldtype`,`htmlcode`) VALUES
('2','lang1','Language Selection Through Model','3','dropDownList','{\"data\":{\"model\":\"AccountName\",\"condition\":\"\",\"id\":\"id\",\"name\":\"name\"},\"html_options\":[]}');
INSERT INTO `settings` (`id`,`unit`,`description`,`value`,`fieldtype`,`htmlcode`) VALUES
('3','language','Language Selection Through Code','en','dropDownCode','{\"data\":{\"ar\":\"Arabic\",\"en\":\"English\"},\"html_options\":[]} ');
INSERT INTO `settings` (`id`,`unit`,`description`,`value`,`fieldtype`,`htmlcode`) VALUES
('4','lang3','Language Selection Through Text Box','ar','textField','{\"html_options\":{\"size\":10,\"maxlength\":2}} ');
INSERT INTO `settings` (`id`,`unit`,`description`,`value`,`fieldtype`,`htmlcode`) VALUES
('5','total_rooms','Total rooms of the hotel','34','','');



-- -------------------------------------------
-- TABLE DATA user
-- -------------------------------------------
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('1','admin','cre@tive_xyz','admin@maaliksoft.com','1','1');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('2','owner','owner@786','owner@gmail.com','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('6','qasim','qasim','qasim@maaliksoft.com','1','1');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('7','ali','123456','ali@yahoo.com','1','1');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('12','bluelines','123456','bluelines@yahoo.com','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('13','branchadmin','branchadmin','faisal@yahoo.com','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('14','frontdesk','frontdesk','test@tesd.com','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('15','backoffice','backoffice','adsfsdf@sdfdsf.sdf','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('16','auditor','auditor','test@tesd.com','4','3');
INSERT INTO `user` (`id`,`username`,`password`,`email`,`hotel_id`,`hotel_branch_id`) VALUES
('17','secret','secret','test@gmail.com','4','3');



-- -------------------------------------------
-- TABLE DATA user_admin
-- -------------------------------------------
INSERT INTO `user_admin` (`id`,`name`,`email`,`user_name`,`password`,`created_date`) VALUES
('1','Faisal','faisal@adad.xom','admin','cre@tive_xyz','2017-03-08 14:03:42');



-- -------------------------------------------
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;
-- -------------------------------------------
-- -------------------------------------------
-- END BACKUP
-- -------------------------------------------
