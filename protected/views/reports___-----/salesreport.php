<head>
	
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/obidatatable.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style type="text/css">
	.form-inline .form-group {
    
     vertical-align: none; 
}
</style>
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<?php

$branch_id = yii::app()->user->branch_id;	 
$branch_info = HmsBranches::model()->findAll("branch_id=$branch_id");
	foreach($branch_info as $b_info){
		$branch_address = $b_info['branch_address'];
        $branch_phone = $b_info['branch_phone'];
	    $branch_fax = $b_info['branch_fax'];
	    $branch_email = $b_info['branch_email'];
	    $hotel_id = $b_info['hotel_id'];
			$hotel_info = HotelTitle::model()->findAll("id=$hotel_id");
 			foreach($hotel_info as $h_info){
	 		$hotel_title = $h_info['title'];
			$hotel_website = $h_info['website'];
			$hotel_logo_image = $h_info['logo_image'];
	 		}
	 }	
	$branch_address .= "<br> Phone: $branch_phone Fax: $branch_fax <br> email: $branch_email";
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
	if(isset($from_date) and isset($to_date)){
		$showdate="<br/> From:  ".date("j F, Y H:i", strtotime($from_date))."\t TO:  ".date("j F, Y H:i",strtotime($to_date));
	}
	$from_date = explode(" ",$_POST['from_date']);
	$to_date = explode(" ",$_POST['to_date']);
	$from_date = $from_date[0]." 00:00:00";
	$to_date = $to_date[0]." 00:00:00";
	// echo "---".$from_date."--".$to_date."--"."(c_date Between '$from_date' And '$to_date') And branch_id = $branch_id Order By id";
	//$_SESSION['gst_show'] = 'to do';
	$sval = Yii::app()->session['taxcontrol'];
	if(isset($sval) && $sval=='ON') $gst_show = " AND ci.gst_show > 0";
	else $gst_show = "";	 
 ?>
<button id="btnExport" onclick="fnExcelReport();"> EXPORT </button>
<button id="btnExport" onclick="printthis(this.id);"> PRINT </button>
<div class="container_mce">
	<table width="100%" border="0">
	<tr>
    	<td rowspan="3">&nbsp;</td>
    	<td rowspan="3"><img src="<?php echo Yii::app()->request->baseUrl; ?>/hotel_logos/<?php echo $hotel_logo_image;?>" height="98px" width="75px" /></td>
    	<td colspan="3" align="center">&nbsp;<font size="+2"><strong><?php echo ucwords($hotel_title); ?></strong></font></td>
        <td rowspan="3" valign="top"><strong>Date</strong></td>
    	<td rowspan="3" valign="top"><?php echo date('j F, Y H:i:s');?></td>
   </tr>
  	<tr>
    	<td colspan="3" align="center"><strong><?php echo ucwords($branch_address); ?></strong></td>
   </tr>
   <tr>    
   		<td colspan="3" align="center"><strong>Sales Report From: <?php echo $showdate; ?></strong></td>
  </tr>
</table>
	<div style="width:100%; border-bottom: solid 1px #000; margin:5px 0;"></div>

<div class="container">
<div class="row">
<div class="col-sm-12">
	<form class="form-inline" action="<?php echo Yii::app()->request->baseUrl;?>/reports/search" method="post">
    <div class="form-group">
    	<div class="col-sm-2">
      <label for="email">Name:</label>
      <input type="text" class="form-control" id="email" placeholder="Enter Name" name="name">
    </div>
</div>
    <div class="form-group">
    	<div class="col-sm-2">
      <label for="email">Room:</label>
      <input type="text" class="form-control" id="email" placeholder="Enter Room" name="room">
    </div>
</div>
<div class="form-group">
    	<div class="col-sm-2">
      <label for="email">Folio:</label>
      <input type="text" class="form-control" id="email" placeholder="Enter Folio" name="folio">
    </div>
</div>

 <input type="submit" name="mysubmit" value="Search" class="btn btn-info" style="margin-top: 22px;">
   
  </form>
  </div>
</div>
</div>

<br>
  

<table id="salestable" width="100%" border="1" align="center">
  <thead>
      <tr>
      	<td></td>
        <td><strong>Sr#</strong></td>
		<td><strong>Guest Name</strong></td>
		<td><strong>Room</strong></td>
        <td><strong>S Code</strong></td>
        <td><strong>Service</strong></td>
        <td><strong>Remarks</strong></td> 
        <td><strong>Folio No.</strong></td> 
        <td><strong>Date</strong></td>       
        <td><strong>Dr</strong></td>
        <td><strong>Cr</strong></td>   
        <td><strong>Balance</strong></td> 
      </tr>
      </thead>
      <tbody>

      <?php

      //$GuestLedger = GuestLedger::model()->findAll("(c_date Between '$from_date' And '$to_date') And branch_id = $branch_id Order By id");

	  if($from_date==$to_date){
		  $from_date = date('Y-m-d', strtotime($from_date));
		  $daterange = " DATE(gl.c_date) = '$from_date'";
	  }
	  else{
		  $from_date = date('Y-m-d', strtotime($from_date));
		  $to_date = date('Y-m-d', strtotime($to_date));
		   $daterange = " DATE(gl.c_date) >= '$from_date' AND DATE(gl.c_date) <= '$to_date'";
	  }

	  // this query user tax control from checkin info table i.e gst_show value

	  
     if(!empty($myname)) {
	     

       
	  	 $sql = "";
	  $sql = "select gl.*, ci.guest_company_id, ci.gst_show, ci.guest_folio_no 
	  from hms_guest_ledger gl";
	  $sql .= " Left join hms_checkin_info ci";
	  $sql .= " ON gl.chkin_id = ci.chkin_id";
	  $sql .= " where 1 ".$gst_show ." and guest_name LIKE'".$myname."%' order by c_date desc";
	 $GuestLedger = Yii::app()->db->createCommand($sql)->queryAll();
   
	  }
    elseif(!empty($myroom))
      {

         $roomresult = Yii::app()->db->createCommand('select mst_room_id from hms_room_master where mst_room_name="'.$myroom.'"')->queryRow();
         
	  $sql = "";
	  $sql = "select gl.*, ci.guest_company_id, ci.gst_show, ci.guest_folio_no 
	  from hms_guest_ledger gl";
	  $sql .= " Left join hms_checkin_info ci";
	  $sql .= " ON gl.chkin_id = ci.chkin_id";
	  $sql .= " where 1 ".$gst_show ." and ci.room_id='".mysql_real_escape_string($roomresult['mst_room_id'])."' order by c_date desc";
	 $GuestLedger = Yii::app()->db->createCommand($sql)->queryAll();
   /*echo"<pre>"; print_r($GuestLedger);echo"</pre>";exit;*/
	}
  elseif(!empty($myfolio))
  {
   $sql = "";
    $sql = "select gl.*, ci.guest_company_id, ci.gst_show, ci.guest_folio_no 
    from hms_guest_ledger gl";
    $sql .= " Left join hms_checkin_info ci";
    $sql .= " ON gl.chkin_id = ci.chkin_id";
    $sql .= " where 1 ".$gst_show ." and ci.chkin_id='".$myfolio."' order by c_date desc";
   $GuestLedger = Yii::app()->db->createCommand($sql)->queryAll(); 
  }
  else
  {
    $sql = "";
    $sql = "select gl.*, ci.guest_company_id, ci.gst_show, ci.guest_folio_no 
    from hms_guest_ledger gl";
    $sql .= " Left join hms_checkin_info ci";
    $sql .= " ON gl.chkin_id = ci.chkin_id";
    $sql .= " where 1 ".$gst_show ." and $daterange  order by c_date desc";
   $GuestLedger = Yii::app()->db->createCommand($sql)->queryAll();
  }
	 /*echo "<pre>";
	 print_r($GuestLedger);
	 echo "</pre>";
	 exit;*/

 foreach($GuestLedger as $rs){
	 $i++;
	 $guest_name = $rs['guest_name'];
     $mst_room_name = RoomMaster::model()->find("mst_room_id=".$rs['room_id'])->mst_room_name;
	if(!empty($rs['guest_company_id']))
	{
     $compname = Company::model()->find("comp_id=".$rs['guest_company_id'])->comp_name;
	}else
	{
		$compname ='';
	}
     $service = Services::model()->find("service_id=".$rs['service_id'])->service_description;
	 $service_code = Services::model()->find("service_id=".$rs['service_id'])->service_code;
	 $remarks = $rs['remarks'];
	 $c_date = $rs['c_date'];
	 $dr = $rs['debit'];
	 $cr = $rs['credit'];	;
  

   
	 $balance = $rs['balance'];
	 $total_dr +=$dr;
	 $total_cr +=$cr;
    
    
     $remaining1=$total_dr-$total_cr;
   
   
	 $guest_folio_no = $rs['guest_folio_no'];
	  $gst_folio_no = $rs['gst_show'];	
    $myquery="select SUM(debit) as debit,SUM(credit) as credit from hms_guest_ledger where chkin_id=".$guest_folio_no.""; 
    $rs2 = Yii::app()->db->createCommand($myquery)->queryAll();
   
   
     if($service=='Advance')
     {
      $remaining=$rs2[0]['debit']-$rs2[0]['credit'];
     }
     else
     {
      $remaining=0;;
     }
    
    
    ?>
    <tr class="text">
      <?php
	  $first_comp = $compname;
	  if(empty($gst_show))	{ $folio_no =  $guest_folio_no; }
	  else $folio_no = $gst_folio_no;		  
	  if($first_comp!=$second_comp)
	  {
	  	$second_comp = $first_comp;
	?>
   
    	 <td align="left" style="font-size:13px"><strong><?php echo $compname;?></strong></td>
    
    <?php }else {?>

      <td></td>
      <?php } ?>
        <td valign="top"><?php echo $i;?></td>
        <td valign="top"><?php echo ucwords($guest_name);?></td>
        <td><?php echo $mst_room_name;?></td>
         <td><?php echo $service_code;?></td>
        <td><?php echo $service;?></td>
        <td><?php echo $remarks;?></td>
        <td><?php echo $folio_no;?></td>
        <td><?php echo date("d/m/y",strtotime($c_date));?></td>
        <td><?php echo $dr;?></td>       
        <td><?php echo $cr;?></td>	
        <td><?php echo $remaining;?></td>  
            
    </tr>
      <?php
	  $second_comp = $first_comp;
 	}
	  ?>
  </tbody>
  <tfoot>
	  <tr>
        <td colspan="9" style="text-align: right;"><strong>Total:</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><strong><?php echo number_format($total_dr,0);?></strong></td>
        <td><strong><?php echo number_format($total_cr,0);?></strong></td>
        <td><strong><?php echo number_format($remaining1,0);?></strong></td>
        
    </tr>
    </tfoot>

  </table>
</div>
<hr />
  </div>

<script>
    function fnExcelReport(){
        var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange; var j=0;
        tab = document.getElementById('salestable'); // id of table

        for(j = 0 ; j < tab.rows.length ; j++)
        {
            tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text=tab_text+"</table>";;
        tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
        tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
        tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
        {
            txtArea1.document.open("txt/html","replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa=txtArea1.document.execCommand("SaveAs",true,"Sales Report.xls");
        }
        else                 //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }
</script>

 <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatablejquery.js"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->

  <script type="text/javascript">
    $(document).ready(function(){

       $("#salestable").DataTable({
        "order":[[7,"desc"]],
       });
    });
  </script>

