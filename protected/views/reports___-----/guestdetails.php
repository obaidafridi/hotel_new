
<?php
$user_id = yii::app()->user->id;
$user_info = User::model()->findAll("id=$user_id");
 foreach($user_info as $u_info){
	 $user_name = $u_info['username'];
	 }
$branch_id = yii::app()->user->branch_id;	 
$branch_info = HmsBranches::model()->findAll("branch_id=$branch_id");
 foreach($branch_info as $b_info){
	 $branch_address = $b_info['branch_address'];
	 $branch_phone = $b_info['branch_phone'];
	 $branch_fax = $b_info['branch_fax'];
	 $branch_email = $b_info['branch_email'];
	 $hotel_id = $b_info['hotel_id'];
	 		
			$hotel_info = HotelTitle::model()->findAll("id=$hotel_id");
 			foreach($hotel_info as $h_info){
	 		$hotel_title = $h_info['title'];
			$hotel_website = $h_info['website'];
			$hotel_logo_image = $h_info['logo_image'];
	 		}
	 }
$branch_address .= "<br> Phone: $branch_phone Fax: $branch_fax <br> email: $branch_email";

$sval = Yii::app()->session['taxcontrol'];
if(isset($sval) && $sval=='ON') $gst_show = " AND gst_show > 0";
else $gst_show = "";


 $cond = '1';
if(isset($_POST[dob_date]) && !empty($_POST[dob_date])){
    $from = date('m', strtotime($_POST[dob_date]));
    $cond = " created_date LIKE '%-".$from."-%'";
}
?>
 
<div class="container_mce">
  <table width="100%" border="0">
  
<!-- <tr>    
    <td colspan="3" align="center" style="font-size: 12px;"><strong>GUEST HISTORY REPORT</strong></td>    
  </tr>-->
  <tr>
    <td rowspan="3">&nbsp;</td>
    <td rowspan="3"><img src="<?php echo Yii::app()->request->baseUrl; ?>/hotel_logos/<?php echo $hotel_logo_image;?>" height="98px" width="75px" /></td>
    <td colspan="3" align="center">&nbsp;<font size="+2"><strong><?php echo ucwords($hotel_title); ?></strong></font></td>
    
    <td rowspan="3"><strong>Date</strong></td>
    <td rowspan="3"><?php echo date('j F, Y H:i:s');?></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><strong><?php echo ucwords($branch_address); ?></strong></td>
  </tr>
  <tr>    
    <td colspan="3" align="center"><strong>GUEST DETAIL REPORT <?php echo $showdate; ?></strong></td>
  </tr>
  <tr>
    <td colspan="3" align="center"><strong><!--LONG STAYING IN-HOUSE REPORT--></strong></td> 
  </tr>
</table>
  <div style="width:100%; border-bottom: solid 1px #000; margin:5px 0;"></div>
  
  
  <table width="100%" border="1">
    <tr>
     <td><strong>Sr#</strong></td>         
       <td align="left"><strong>Guest Name</strong></td>
       <td align="left"><strong>Company</strong></td> 
       <td align="left"><strong>Created Date</strong></td>
       <td align="left"><strong>Mobile</strong></td>
       <td align="left"><strong>Email</strong></td>         
    </tr>
    <?php
	
    $rs = Yii::app()->db->createCommand("SELECT  `guest_name`, `guest_mobile`,  `guest_email`, `guest_company_id`, `created_date` FROM `hms_guest_info` WHERE $cond ORDER BY DATE_FORMAT(`created_date`, '%d')  ASC")->queryAll();
    $i=0;
    foreach($rs as $row){
        $i++;	
        $cmp_name = Yii::app()->db->createCommand("SELECT  `comp_name` FROM `hms_company_info` WHERE comp_id='".$row[guest_company_id]."'")->queryScalar();
    
    ?>
    <tr class="text">
        <td align="left"><?php echo $i;?></td>
        <td style="padding:5px;" align="left"><?php echo strtoupper($row[guest_name]);?></td>
        <td style="padding:5px;" align="left"><?php echo $cmp_name;?></td>
        <td style="padding:5px;" align="left"><?php echo date('d-m-Y',  strtotime($row[created_date])); ?></td>
        <td style="padding:5px;" align="left"><?php echo $row[guest_mobile]; ?></td>
        <td style="padding:5px;" align="left"><?php echo $row[guest_email]?></td>
     
    </tr>
    <?php } ?>
  </table>
  <div style="clear:both"></div>
    
  <p align="right" style=" margin-top:40px; padding-right:10px;"><span style="float:left; font-size:12px;">Print Time: <?php echo date("d/m/y H:i"); ?></span> <input id="print" type="button" value="Print" onclick="printthis(this.id)" />    </p>
</div>